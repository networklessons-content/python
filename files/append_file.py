with open("demo2.txt", "a") as text_file:
    text_file.write("\nWriting content to this file")

with open("demo2.txt") as text_file:
    lines = text_file.readlines()
    
print(lines)
