def test_global_keyword():
    global test_string
    test_string = "Global variable."


test_global_keyword()
print("Executed globally: " + test_string)
